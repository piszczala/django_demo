build:
	docker-compose build

run:
	docker-compose up -d

migrate:
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate

stop:
	docker-compose down

debug:
	docker attach djangodemo_web_1

bash:
	docker-compose exec web bash

setup: build run migrate