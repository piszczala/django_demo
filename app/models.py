from django.db import models


class User(models.Model):
    name = models.CharField(max_length=48)
    avatar_url = models.CharField(max_length=1024)
    description = models.CharField(max_length=1024)


class Photo(models.Model):
    description = models.CharField(max_length=1024)
    photo_url = models.CharField(max_length=1024)
    data = models.DateField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return f"<{self.data} {self.photo_url}>"

    def __repr__(self):
        return f"<{self.data} {self.photo_url}>"
