from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
import datetime
from .models import User, Photo
from .image import get_image_urls_from_duckduckgo


def hello(request):
    return HttpResponse("Hello, world")


def front(request):
    last_photos = Photo.objects.all().order_by("-id")[:10]
    context = {"photos": last_photos}
    return render(request, "index.html", context)


def profile(request, username):
    try:
        x = User.objects.get(name=username)
        y = x.photo_set.all()
        context = {"avatar": x.avatar_url, "username": x.name, "photos": y}
        return render(request, "user.html", context)
    except ObjectDoesNotExist:
        avatar_url = get_image_urls_from_duckduckgo(username, 2)[0]
        x = User(name=username, avatar_url=avatar_url, description="Nice description")
        x.save()
        context = {"avatar": x.avatar_url, "username": x.name, "photos": []}
        return render(request, "user.html", context)


def add_new_photo(request, username):
    author = User.objects.get(name=username)
    photos_number = len(author.photo_set.all())
    photo_url = get_image_urls_from_duckduckgo(username, photos_number + 3)[-1]
    description = ''
    new_photo = Photo(author=author, photo_url=photo_url, description=description)
    new_photo.save()
    print(new_photo.id)
    return redirect("profile", username=username)


def like(request, photo_id):
    photo = Photo.objects.get(id=photo_id)
    photo.likes += 1
    photo.save()
    return redirect("front")
